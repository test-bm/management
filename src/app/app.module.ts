import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from '@env/environment';
import { AppComponent } from './app.component';
import { ServicesModule } from '@services/services.module';
import { PrivateContentModule } from './main/private-content/private-content.module';
import { AppAuthModule } from './main/app-auth/app-auth.module';
import { GuardsModule } from '@guards/guards.module';
import { ApplicationService } from '@services/application/application.service';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ComponentsModule } from '@components/components.module';

const appRoutes: Routes = [
    {
        path: '**',
        redirectTo: 'auth/login',
    },
];

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        HttpClientModule,
        BrowserAnimationsModule,
        RouterModule.forRoot(appRoutes, { useHash: true }),
        ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: environment.production,
        }),

        // Módulo de componentes compartido
        ComponentsModule,

        // Módulo de servicios de la app
        ServicesModule,

        // Módulo para el contenido privado
        PrivateContentModule,

        // Módulo para la autenticación
        AppAuthModule,

        // Módulo para los servicios de guardias de rutas
        GuardsModule,

        MatProgressBarModule,
        MatSnackBarModule
    ],
    providers: [ ApplicationService ],
    bootstrap: [ AppComponent ],
})
export class AppModule {}
