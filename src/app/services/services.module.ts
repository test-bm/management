import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OauthServiceModule } from './oauth/oauth-service.module';
import { UserServiceModule } from './user/user-service.module';
import { CustomerServiceModule } from './customer/customer-service.module';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        OauthServiceModule,
        UserServiceModule,
        CustomerServiceModule
    ]
})
export class ServicesModule { }
