import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { OauthFacadeService } from './oauth/oauth-facade/oauth-facade.service';

export abstract class AbstractAuthorization {

    protected unsubscribeAll = new Subject<boolean>();
    protected authorizationHeader: { Authorization: string } = { Authorization: '' };

    protected constructor(protected http: HttpClient, protected oauthFacadeService: OauthFacadeService ) {
        this.oauthFacadeService
        .tokenWithType
        .pipe(takeUntil(this.unsubscribeAll))
        .subscribe(token => {
            if( token ) {
                this.authorizationHeader.Authorization = token;
            }
        });
    }

}
