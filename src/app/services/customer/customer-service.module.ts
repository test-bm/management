import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonService } from './common-service/common.service';
import { CustomerService } from './customer-service/customer.service';

@NgModule({
    declarations: [],
    imports: [CommonModule],
    providers: [
        CommonService,
        CustomerService,
    ]
})
export class CustomerServiceModule {}
