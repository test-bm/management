import { Injectable } from '@angular/core';
import { AbstractAuthorization } from '@services/abstract-authorization';
import { HttpClient } from '@angular/common/http';
import { OauthFacadeService } from '@services/oauth/oauth-facade/oauth-facade.service';
import { CustomerReport, Customer } from '@models/customer/customer';
import { environment } from '@env/environment';
import { PageResultModel } from '@models/common/pagination';

@Injectable()
export class CustomerService extends AbstractAuthorization {
    constructor(http: HttpClient, oauthFacadeService: OauthFacadeService) {
        super(http, oauthFacadeService);
    }

    getCustomerById(id: number): Promise<CustomerReport> {
        const url = `${environment.api.baseUrlManagementWS}customer/${id}`;
        return this.http
            .get<CustomerReport>(url, { headers: this.authorizationHeader })
            .toPromise();
    }

    getAllCustomers(
        pageNumber: number,
        pageSize: number,
        search: string
    ): Promise<PageResultModel<CustomerReport>> {
        const url = `${environment.api.baseUrlManagementWS}customer/all`;
        return this.http
            .get<PageResultModel<CustomerReport>>(url, {
                headers: this.authorizationHeader,
                params: {
                    pageNumber: pageNumber.toString(),
                    pageSize: pageSize.toString(),
                    search,
                },
            })
            .toPromise();
    }

    createCustomer(
        isIndividualCustomer: boolean,
        customer: Customer
    ): Promise<number> {
        const url = `${environment.api.baseUrlManagementWS}customer`;
        return this.http
            .post<number>(url, customer, {
                headers: this.authorizationHeader,
                params: {
                    isIndividualCustomer: isIndividualCustomer.toString(),
                },
            })
            .toPromise();
    }

    updateCustomer(customer: Customer): Promise<number> {
        const url = `${environment.api.baseUrlManagementWS}customer`;
        return this.http
            .put<number>(url, customer, { headers: this.authorizationHeader })
            .toPromise();
    }

    deleteCustomer(id: number): Promise<void> {
        const url = `${environment.api.baseUrlManagementWS}customer`;
        return this.http
            .delete<any>(url, {
                headers: this.authorizationHeader,
                params: { id: id.toString() },
            })
            .toPromise();
    }
}
