import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OauthFacadeService } from '@services/oauth/oauth-facade/oauth-facade.service';
import { AbstractAuthorization } from '@services/abstract-authorization';
import { DocumentType } from '@models/common/document';
import { environment } from '@env/environment';
import { State } from '@models/common/address';

@Injectable()
export class CommonService extends AbstractAuthorization {
    constructor(http: HttpClient, oauthFacadeService: OauthFacadeService) {
        super(http, oauthFacadeService);
    }

    getDocumentTypes(countryCode: string): Promise<Array<DocumentType>> {
        const url = `${environment.api.baseUrlManagementWS}common`;
        return this.http
            .get<Array<DocumentType>>(url, {
                headers: this.authorizationHeader,
                params: {
                    countryCode,
                },
            })
            .toPromise();
    }

    getStates(countryCode: string): Promise<Array<State>> {
        const url = `${environment.api.baseUrlManagementWS}common/states`;
        return this.http
            .get<Array<State>>(url, {
                headers: this.authorizationHeader,
                params: {
                    countryCode,
                },
            })
            .toPromise();
    }
}
