import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OauthFacadeService } from '@services/oauth/oauth-facade/oauth-facade.service';
import { AbstractAuthorization } from '@services/abstract-authorization';
import { AccountReport, Account } from '@models/user/account';
import { environment } from '@env/environment';
import { PageResultModel } from '@models/common/pagination';

@Injectable()
export class UserService extends AbstractAuthorization {
    constructor(http: HttpClient, oauthFacadeService: OauthFacadeService) {
        super(http, oauthFacadeService);
    }

    getUserById(id: number): Promise<AccountReport> {
        const url = `${environment.api.baseUrlManagementWS}user`;
        return this.http
            .get<AccountReport>(url, { headers: this.authorizationHeader })
            .toPromise();
    }

    getAllUsers(
        pageNumber: number,
        pageSize: number,
        search: string
    ): Promise<PageResultModel<AccountReport>> {
        const url = `${environment.api.baseUrlManagementWS}user/all`;
        return this.http
            .get<PageResultModel<AccountReport>>(url, {
                headers: this.authorizationHeader,
                params: {
                    pageNumber: pageNumber.toString(),
                    pageSize: pageSize.toString(),
                    search
                },
            })
            .toPromise();
    }

    createUser(account: Account): Promise<number> {
        const url = `${environment.api.baseUrlManagementWS}user`;
        return this.http
            .post<number>(url, account, { headers: this.authorizationHeader })
            .toPromise();
    }

    updateUser(account: Account): Promise<number> {
        const url = `${environment.api.baseUrlManagementWS}user`;
        return this.http
            .put<number>(url, account, { headers: this.authorizationHeader })
            .toPromise();
    }

    deleteUser(id: number): Promise<void> {
        const url = `${environment.api.baseUrlManagementWS}user`;
        return this.http
            .delete<any>(url, {
                headers: this.authorizationHeader,
                params: { id: id.toString() },
            })
            .toPromise();
    }
}
