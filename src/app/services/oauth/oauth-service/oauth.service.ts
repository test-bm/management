import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Account } from '@models/user/account';
import { BehaviorSubject, Observable } from 'rxjs';
import { OAuthUserInfo } from '@models/user/oauth';
import { TOKEN_KEY } from '@constants/application';
import { environment } from '@env/environment';

@Injectable()
export class OauthService {

    private readonly tokenWithTypeSubject = new BehaviorSubject<string>(null);
    private readonly currentUserAuthenticatedSubject = new BehaviorSubject<OAuthUserInfo>(null);

    constructor(
        private readonly httpClient: HttpClient
    ) {}

    token(): Observable<string> {
        return this.tokenWithTypeSubject.asObservable();
    }

    /**
     * Observable para los datos del usuario autenticado
     */
    currentUserAuthenticated(): Observable<OAuthUserInfo> {
        return this.currentUserAuthenticatedSubject.asObservable();
    }

    isAuthenticated(): boolean {
        const sessionData = JSON.parse(localStorage.getItem(TOKEN_KEY)) as OAuthUserInfo;
        if( sessionData ) {
            this.currentUserAuthenticatedSubject.next(sessionData);
            this.tokenWithTypeSubject.next(`Bearer ${sessionData.accessToken}`);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Endpoint para autenticarse en el sistema
     * @param user
     */
    authenticate(user: Account): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                const url = `${ environment.api.baseUrlManagementWS }auth/login`;
                const sessionData = await this.httpClient
                .post<OAuthUserInfo>(url, user)
                .toPromise();

                localStorage.setItem(TOKEN_KEY, JSON.stringify(sessionData));

                this.tokenWithTypeSubject.next(`Bearer ${sessionData.accessToken}`);
                this.currentUserAuthenticatedSubject.next(sessionData);

                resolve();
            } catch(e) {
                console.error(e);
                reject(e);
            }
        });
    }

    logout(): void {
        localStorage.removeItem(TOKEN_KEY);
        this.tokenWithTypeSubject.next(null);
        this.currentUserAuthenticatedSubject.next(null);
    }

}
