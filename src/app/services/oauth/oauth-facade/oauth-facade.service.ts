import { Injectable, Injector } from '@angular/core';
import { OauthService } from '@services/oauth/oauth-service/oauth.service';
import { OauthUserService } from '@services/oauth/oauth-user-service/oauth-user.service';
import { Observable } from 'rxjs';
import { OAuthUserInfo } from '@models/user/oauth';
import { Account } from '@models/user/account';

@Injectable()
export class OauthFacadeService {

    private lazyOauthService: OauthService;
    private lazyOauthUserService: OauthUserService;

    constructor(
        private readonly injector: Injector
    ) {}

    private get oauthService(): OauthService {
        if ( !this.lazyOauthService ) {
            this.lazyOauthService = this.injector.get(OauthService);
        }
        return this.lazyOauthService;
    }

    private get oauthUserService(): OauthUserService {
        if ( !this.lazyOauthUserService ) {
            this.lazyOauthUserService = this.injector.get(OauthUserService);
        }
        return this.lazyOauthUserService;
    }

    get tokenWithType(): Observable<string> {
        return this.oauthService.token();
    }

    isAuthenticated(): boolean {
        return this.oauthService.isAuthenticated();
    }

    get currentUserAuthenticated(): Observable<OAuthUserInfo> {
        return this.oauthService.currentUserAuthenticated();
    }

    login(user: Account): Promise<void> {
        return this.oauthService.authenticate(user);
    }

    logout(): void {
        this.oauthService.logout();
    }

    registerUser(user: Account): Promise<number> {
        return this.oauthUserService.registerUser(user);
    }

}
