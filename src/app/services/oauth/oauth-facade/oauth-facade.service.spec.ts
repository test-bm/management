import { TestBed } from '@angular/core/testing';

import { OauthFacadeService } from './oauth-facade.service';

describe('OauthFacadeService', () => {
  let service: OauthFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OauthFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
