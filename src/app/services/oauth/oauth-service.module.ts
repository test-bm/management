import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OauthFacadeService } from './oauth-facade/oauth-facade.service';
import { OauthUserService } from './oauth-user-service/oauth-user.service';
import { OauthService } from './oauth-service/oauth.service';

@NgModule({
    declarations: [],
    imports: [CommonModule],
    providers: [
        OauthFacadeService,
        OauthUserService,
        OauthService
    ]
})
export class OauthServiceModule {}
