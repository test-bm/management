import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Account } from '@models/user/account';
import { environment } from '@env/environment';

@Injectable()
export class OauthUserService {
    constructor(private readonly http: HttpClient) {
    }

    /**
     * Endpoint para registrar nuevo usuario
     * @param user
     */
    registerUser(user: Account): Promise<number> {
        const url = `${environment.api.baseUrlManagementWS}user`;
        return this.http.post<number>(url, user).toPromise();
    }
}
