import { Role } from './role';

export interface Account {
    id?: number;
    email: string;
    password: string;
    name?: string;
    sub?: string;
    role?: Role
}

export interface AccountReport {
    email: string;
    name: string;
    sub?: string;
    roleName: string;
}
