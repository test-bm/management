
export interface OAuthUserInfo {
    name: string;
    email: string;
    accessToken: string;
    roleName: string;
}
