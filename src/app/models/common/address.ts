
export interface Address {
    id: number;
    addressDescription: string;
    number: string;
    zoneNumber: string;
    street: string;
    residential: string;
    state: State;
}

export interface State {
    id: number;
    code: string;
    name: string;
    country: Country;
}

export interface Country {
    id: number;
    code: string;
    name: string;
}
