export interface PageResultModel<T> {
    content?: Array<T>;
    last?: boolean;
    totalPages?: number;
    totalElements?: number;
    numberOfElements?: number;
    first?: boolean;
    size?: number;
    number?: number;
    empty?: boolean;
}
