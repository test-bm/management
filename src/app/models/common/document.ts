import { Country } from './address';

export interface Document {
    id: number;
    number: string;
    documentType;
}

export interface DocumentType {
    id: number;
    name: string;
    pattern: string;
    country: Country;
}

export const DOCUMENT_TYPE_CODE: Array<{
    code: string;
    description: string;
}> = [
    { code: 'ID', description: 'ID' },
    { code: 'DRIVER_LICENCE', description: 'DRIVER_LICENCE' },
    { code: 'PASSPORT', description: 'PASSPORT' },
    { code: 'PATENT', description: 'PATENT' },
];

export const DOCUMENT_TYPE_CATEGORY: Array<{
    code: string;
    description: string;
}> = [
    { code: 'IND', description: 'INDIVIDUAL' },
    { code: 'JUR', description: 'JURIDICA' }
];
