import { Address } from '@models/common/address';
import { Document } from '@models/common/document';

export interface Customer {
    id: number;
    name: string;
    lastName: string;
    fullName: string;
    legalRepresentative: string;
    phoneNumber: string;
    birthdate: Date;
    email: string;
    address: Address;
    document: Document;
}

export interface CustomerReport {
    id: number;
    fullName: string;
    legalRepresentative: string;
    phoneNumber: string;
    birthdate: Date;
    email: string;
    documentId: number;
    documentNumber: string;
    addressId: number;
    addressDescription: string;
}

export const GENDER: Array<{code: string; description: string}> = [
    { code: 'M', description: 'Masculino' },
    { code: 'F', description: 'Femenino' }
];
