import { Injectable } from '@angular/core';
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    Router,
} from '@angular/router';
import { OauthFacadeService } from '@services/oauth/oauth-facade/oauth-facade.service';

@Injectable()
export class AuthContentGuard implements CanActivate {
    constructor(
        private readonly router: Router,
        private oauthFacadeService: OauthFacadeService
    ) {}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Promise<boolean> {
        return new Promise<boolean>(async (resolve) => {
            const isAuthenticated = this.oauthFacadeService.isAuthenticated();

            if (isAuthenticated) {
                await this.router.navigate(['/dashboard']);
                resolve(false);
            }

            resolve(true);
        });
    }
}
