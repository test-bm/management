import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { OauthFacadeService } from '@services/oauth/oauth-facade/oauth-facade.service';

@Injectable()
export class PrivateContentGuard implements CanActivate {

    constructor(
        private readonly router: Router,
        private readonly oauthFacadeService: OauthFacadeService
    ) {}

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
        return new Promise<boolean>(async resolve => {
            const isAuthenticated = this.oauthFacadeService.isAuthenticated();

            if(!isAuthenticated) {
                await this.router.navigate(['/login']);
                resolve(false);
            }

            resolve(true);
        });
    }
}
