import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { OauthFacadeService } from '@services/oauth/oauth-facade/oauth-facade.service';
import { ApplicationService } from '@services/application/application.service';
import { Account } from '@models/user/account';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

    showProgressBar = false;
    loginForm: FormGroup;

    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly router: Router,
        private readonly oAuthFacadeService: OauthFacadeService,
        private readonly applicationService: ApplicationService
    ) {
        this.loginForm = this.formBuilder.group({
            email: new FormControl('', [ Validators.required, Validators.email ]),
            password: new FormControl('', [ Validators.required ])
        });
    }

    ngOnInit(): void {
        this.applicationService.hideToolbar();
    }

    doLogin(): void {
        this.showLoading();
        const user: Account = {
            ...this.loginForm.getRawValue()
        };
        this.oAuthFacadeService
        .login(user)
        .then(async () => await this.router.navigateByUrl('/dashboard'))
        .catch((exception: HttpErrorResponse) => {
            if(exception.status === 404) {
                this.applicationService.showSnackBar('Usuario no encontrado', true);
            }

            if(exception.status === 403) {
                this.applicationService.showSnackBar('Contraseña incorrecta', true);
            }
        })
        .finally(() => this.hideLoading());
    }

    private showLoading(): void {
        this.loginForm.disable();
        this.showProgressBar = true;
        this.applicationService.showProgressBar();
    }

    private hideLoading(): void {
        this.loginForm.enable();
        this.showProgressBar = false;
        this.applicationService.hideProgressBar();
    }
}
