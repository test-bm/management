import { Component, OnInit } from '@angular/core';
import { GridApi, GridOptions } from '@ag-grid-community/core';
import { MatDialog, MatSnackBar } from '@angular/material';

@Component({
    selector: 'app-user-list',
    template: `
            <ag-grid-angular
                    style="width: 100%; height: 100%;"
                    class="ag-theme-balham"
                    id="myGrid"
                    [gridOptions]="gridOptions"
                    [columnDefs]="columnDefs"
                    [defaultColDef]="defaultColDef"
                    [autoGroupColumnDef]="autoGroupColumnDef"
                    [rowModelType]="rowModelType"
                    [rowGroupPanelShow]="rowGroupPanelShow"
                    [functionsReadOnly]="true"
                    [animateRows]="true"
                    [pagination]="true"
                    [paginationAutoPageSize]="false"
                    [rowSelection]="rowSelection"
                    [overlayNoRowsTemplate]="overlayNoRowsTemplate"
                    (gridReady)="onGridReady($event)">
        </ag-grid-angular>
    `
})
export class UserListComponent implements OnInit {

    totalNotificationTypes = 0;
    gridApi: GridApi;
    gridOptions: GridOptions = {
        paginationPageSize: 100,
        cacheBlockSize: 100,
        infiniteInitialRowCount: 1,
        cacheOverflowSize: 2,
        rowHeight: 45
    };

    defaultColDef = {
        resizable: true,
    };
    rowSelection = 'multiple';
    rowModelType = 'infinite';
    rowGroupPanelShow = 'never';
    autoGroupColumnDef = { width: 150 };
    overlayNoRowsTemplate = '';

    constructor(
        private readonly dialog: MatDialog,
        private readonly snackBar: MatSnackBar
    ) {}

    ngOnInit(): void {}
}
