import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserListComponent } from './user-list/user-list.component';
import { UserComponent } from './user/user.component';
import { RouterModule, Routes } from '@angular/router';
import { PrivateContentGuard } from '@guards/private-content.guard';
import { AgGridModule } from '@ag-grid-community/angular';

const routes: Routes = [
    {
        path: '',
        component: UserListComponent,
        canActivate: [ PrivateContentGuard ]
    }
];

@NgModule({
    declarations: [ UserListComponent, UserComponent ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        AgGridModule.withComponents([]),
    ],
})
export class UsersModule {}
