import { Component, OnInit, OnDestroy } from '@angular/core';
import { OauthFacadeService } from '@services/oauth/oauth-facade/oauth-facade.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ApplicationService } from '@services/application/application.service';

@Component({
    selector: 'app-dashboard',
    template: `
        <h2>
            Bienvenid@ <strong>{{ userName }}</strong>
        </h2>
    `,
})
export class DashboardComponent implements OnInit, OnDestroy {
    userName = '';

    private readonly destroy$ = new Subject<boolean>();

    constructor(
        private readonly oauthFacadeService: OauthFacadeService,
        private readonly applicationService: ApplicationService,
        ) {}

    ngOnInit(): void {
        this.applicationService.showToolbar();
        this.oauthFacadeService.currentUserAuthenticated
            .pipe(takeUntil(this.destroy$))
            .subscribe((user) => (this.userName = user.name));
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
        this.destroy$.complete();
    }
}
