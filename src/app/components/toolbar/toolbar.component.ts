import { Component, OnInit, OnDestroy } from '@angular/core';
import { APPLICATION_NAME } from '@constants/application';
import { OauthFacadeService } from '@services/oauth/oauth-facade/oauth-facade.service';
import { ApplicationService } from '@services/application/application.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { OAuthUserInfo } from '@models/user/oauth';

@Component({
    selector: 'app-toolbar-layout',
    templateUrl: './toolbar.component.html',
    styleUrls: [ './toolbar.component.scss' ]
})
export class ToolbarComponent implements OnInit, OnDestroy {

    title = APPLICATION_NAME;
    showToolbar = false;
    userInfoAuthenticated: OAuthUserInfo;

    private readonly destroy$ = new Subject<boolean>();

    constructor(
        private readonly oauthFacadeService: OauthFacadeService,
        private readonly applicationService: ApplicationService,
        private readonly router: Router
    ) {
    }

    ngOnInit(): void {
        this.applicationService
            .getStatusToolbar()
            .subscribe(value => this.showToolbar = value);

        this.oauthFacadeService.currentUserAuthenticated
            .pipe(takeUntil(this.destroy$))
            .subscribe((user) => (this.userInfoAuthenticated = user));
    }

    async doLogout(): Promise<void> {
        this.applicationService.showProgressBar();
        this.oauthFacadeService.logout();
        await this.router.navigateByUrl('/auth/login');
        this.applicationService.hideProgressBar();
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
        this.destroy$.complete();
    }
}
