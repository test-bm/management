import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';

@Component({
  selector: 'app-ag-grid-button-remove',
  template: `
    <button
      (click)="onClick()"
      mat-icon-button
      title="{{ commons.removeThisRow }}"
    >
      <mat-icon class="remove-icon-button" aria-label="Remove item"
        >cancel</mat-icon
      >
    </button>
  `,
  styles: [
    `
      .remove-icon-button {
        color: #f44336;
      }
    `,
  ],
})
export class AgGridButtonRemoveComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;

  constructor() {}

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.value, col: params.colDef.headerName };
  }

  refresh(): boolean {
    return false;
  }

  onClick(): void {
    if (this.params) {
      const fn = this.params['onAction'];
      if (fn) {
        fn(this.params.data);
      }
    }
  }
}
