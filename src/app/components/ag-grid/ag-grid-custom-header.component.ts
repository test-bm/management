import { Component, ViewChild, ElementRef } from '@angular/core';
import { IHeaderAngularComp } from '@ag-grid-community/angular';

@Component({
  selector: 'app-ag-grid-custom-header',
  template: `
    <div class="ag-cell-label-container" role="presentation">
      <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button"
        ><span
          *ngIf="params.enableMenu"
          #menuButton
          (click)="onMenuClicked($event)"
          class="ag-icon ag-{{ params.menuIcon }}"
          unselectable="on"
        >
        </span
      ></span>

      <div
        ref="eLabel"
        class="ag-header-cell-label"
        role="presentation"
        unselectable="on"
      >
        <span
          ref="eText"
          class="ag-header-cell-text"
          role="columnheader"
          unselectable="on"
          >{{ params.displayName }}</span
        >
        <span
          ref="eFilter"
          class="ag-header-icon ag-filter-icon ag-hidden"
          aria-hidden="true"
          ><span class="ag-icon ag-icon-filter" unselectable="on"></span
        ></span>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
        width: 100%;
        height: 100%;
      }
    `,
    `
      .customHeaderLabel,
      .customSortDownLabel,
      .customSortUpLabel,
      .customSortRemoveLabel {
        float: left;
        margin: 0 0 0 3px;
      }

      .customHeaderMenuButton {
        float: left;
        margin: 0 0 0 3px;
      }

      .customHeaderLabel {
        float: left;
        margin: 0 0 0 3px;
      }

      .active {
        color: cornflowerblue;
      }

      mat-icon {
        font-size: 14px;
      }
    `,
  ],
})
export class AgGridCustomHeaderComponent implements IHeaderAngularComp {
  @ViewChild('menuButton', { read: ElementRef, static: false })
  public menuButton;

  params: any;
  boolValue = '';
  private ascSort: string;
  private descSort: string;
  private noSort: string;

  constructor() {}

  agInit(params: any): void {
    this.params = params;
    this.params.menuIcon = params.menuIcon ? params.menuIcon : 'icon-menu';
    params.column.addEventListener(
      'sortChanged',
      this.onSortChanged.bind(this)
    );
    this.onSortChanged();
  }

  refresh(): boolean {
    return false;
  }

  onMenuClicked(): void {
    this.params.showColumnMenu(this.menuButton.nativeElement);
  }

  onSortChanged(): void {
    this.ascSort = this.descSort = this.noSort = 'inactive';
    if (this.params.column.isSortAscending()) {
      this.ascSort = 'active';
    } else if (this.params.column.isSortDescending()) {
      this.descSort = 'active';
    } else {
      this.noSort = 'active';
    }
  }

  onSortRequested(order, event): void {
    this.params.setSort(order, event.shiftKey);
  }

}
