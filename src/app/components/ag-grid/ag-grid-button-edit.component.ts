import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';

@Component({
  selector: 'app-ag-grid-button-edit',
  template: `
    <button (click)="onClick()" mat-icon-button title="Editar este registro">
      <mat-icon class="edit-icon-button" aria-label="Edit item">edit</mat-icon>
    </button>
  `,
  styles: [
    `
      .edit-icon-button {
        color: #757575;
      }
    `,
  ],
})
export class AgGridButtonEditComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;

  constructor() {}

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.value, col: params.colDef.headerName };
  }

  refresh(): boolean {
    return false;
  }

  onClick(): void {
    if (this.params) {
      const fn = this.params['onAction'];
      if (fn) {
        fn(this.params.data);
      }
    }
  }
}
