import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { AgGridIconCheckComponent } from '@components/ag-grid/ag-grid-icon-check.component';
import { AgGridButtonRemoveComponent } from '@components/ag-grid/ag-grid-button-remove.component';
import { AgGridButtonEditComponent } from '@components/ag-grid/ag-grid-button-edit.component';
import {
  MatButtonModule,
  MatIconModule,
  MatSlideToggleModule,
} from '@angular/material';
import { AgGridModule } from '@ag-grid-community/angular';
import { AgGridIconButtonActionComponent } from '@components/ag-grid/ag-grid-icon-button-action.component';
import { AgGridSlideToggleComponent } from '@components/ag-grid/ag-grid-slide.component';
import { FormsModule } from '@angular/forms';
import { AgGridCustomHeaderComponent } from '@components/ag-grid/ag-grid-custom-header.component';
import { AgGridFormatDateComponent } from '@components/ag-grid/ag-grid-format-date.component';
import { AgGridCustomDayAsStringComponent } from '@components/ag-grid/ag-grid-custom-day-as-string.component';
import { AgGridCustomDateComponent } from '@components/ag-grid/ag-grid-custom-date.component';

@NgModule({
  declarations: [
    AgGridIconCheckComponent,
    AgGridButtonRemoveComponent,
    AgGridButtonEditComponent,
    AgGridIconButtonActionComponent,
    AgGridSlideToggleComponent,
    AgGridCustomHeaderComponent,
    AgGridFormatDateComponent,
    AgGridCustomDayAsStringComponent,
    AgGridCustomDateComponent,
  ],
  entryComponents: [],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    AgGridModule.withComponents([]),
    FormsModule,
    MatSlideToggleModule,
  ],
  exports: [
    AgGridIconCheckComponent,
    AgGridButtonRemoveComponent,
    AgGridButtonEditComponent,
    AgGridIconButtonActionComponent,
    AgGridSlideToggleComponent,
    AgGridCustomHeaderComponent,
    AgGridFormatDateComponent,
    AgGridCustomDayAsStringComponent,
    AgGridCustomDateComponent,
  ],
  providers: [DatePipe],
})
export class AgGridComponentsModule {}
