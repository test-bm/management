import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';

@Component({
  selector: 'app-ag-grid-custom-date',
  template: `
    <span>{{ newDate }}</span>
  `,
  styles: [
    `
      :host {
        display: flex;
        flex-direction: row;
        align-items: start;
        justify-content: start;
        width: 100%;
        height: 100%;
      }
    `,
  ],
})
export class AgGridCustomDateComponent implements ICellRendererAngularComp {
  public cell: any;
  newDate: any;

  constructor(private datePipe: DatePipe) {}

  agInit(params: any): void {
    if (params.value) {
      const date = moment(params.value);
      this.newDate = this.datePipe.transform(date, 'dd-MM-yyyy');
      this.cell = { col: params.colDef.headerName };
    }
  }

  refresh(): boolean {
    return false;
  }
}
