import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';

@Component({
  selector: 'app-ag-grid-icon-button-action',
  template: `
    <button (click)="onClick()" mat-icon-button [title]="buttonTitle">
      <mat-icon class="action-icon-button" aria-label="action button icon">{{iconName}}</mat-icon>
    </button>
  `,
  styles: [
    `
      .action-icon-button {
        color: #757575;
      }
    `,
  ],
})
export class AgGridIconButtonActionComponent
  implements ICellRendererAngularComp {
  private params: any;
  cell: any;
  iconName = 'info';
  buttonTitle = 'Action button';

  constructor() {}

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.value, col: params.colDef.headerName };
    this.iconName = params.iconName;
    this.buttonTitle = params.buttonTitle;
  }

  refresh(): boolean {
    return false;
  }

  onClick(): void {
    if (this.params) {
      const fn = this.params['onAction'];
      if (fn) {
        fn(this.params.data);
      }
    }
  }
}
