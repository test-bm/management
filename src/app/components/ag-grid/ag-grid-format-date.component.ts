import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';

@Component({
  selector: 'app-ag-grid-format-date',
  template: `
    {{ params.value | date: 'dd/MM/yyyy HH:mm:ss' }}
  `,
  styles: [],
})
export class AgGridFormatDateComponent implements ICellRendererAngularComp {
  params: any;
  public cell: any;

  constructor() {}

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.value, col: params.colDef.headerName };
  }

  refresh(): boolean {
    return false;
  }
}
