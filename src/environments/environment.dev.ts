export const environment = {
    production: false,
    api: {
        baseUrlManagementWS: 'http://localhost:8081/management-ws/api/'
    }
};
